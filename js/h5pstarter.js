(function ($, Drupal, drupalSettings) {

    'use strict';

    Drupal.behaviors.mainMenu = {
      attach: function (context, settings) {

        $(".toggle-filters").on("click", function(e) {
          $("#sidebar-left").addClass("open");
        });
        $(".close-filters").on("click", function(e) {
          $("#sidebar-left").removeClass("open");
        });

      }
    };
  
    Drupal.behaviors.formScripts = {
      attach: function (context, settings) {
        
        // move form actions region to the correct region
        $(".form-actions-append").append( $("div#edit-actions") );
        $(".form-actions-prepend").prepend( $("div#edit-actions") );

        $('#start-h5p-tour').click(function(){
          Tour.run([
            {
              element: $('ul.menu--account li.active a.is-active'),
              content: 'Welcome to eCampusOntario H5P Studio',
              position: 'bottom',
            },
            {
              element: $('#edit-title-wrapper'),
              content: `
              <h2>Start by providing a title for your content</h2>
              <p>This title will be used when displaying your content within this site. Make it descriptive enough for others to easily identify the purpose of your content.</p>
              <p class="small text-muted">Example: "Lymphatic System Vocabulary Quiz"</p>`,
              position: 'right',
              padding: 15,
            },
            {
              element: $('#edit-field-subject-wrapper'),
              content: `
              <h2>What subject does your content cover?</h2>
              <p>Select one or more of the pre-defined subject areas that your content covers to help others discover your content.</p>
              <p class="small text-muted">Example: "Mathematics & Science; Medicine & Nursing"</p>`,
              position: 'right',
              padding: 15,
            },
            {
              element: $('#edit-field-instructor-description-wrapper'),
              content: `<h2>What is the goal or intended use of this content</h2>
              <p>Provide a brief description of your content to help others understand your goals, how you created it, and how someone else might use it.</p>
              <p class="small text-muted">Example: "This formative assessment quiz helps students familiarize themselves with key terms and definitions for the lymphatic system.</p>`,
              position: 'right',
              padding: 15,
            },
            {
              element: $('#edit-field-tags-wrapper'),
              content: `<h2>Group your content using keywords</h2>
              <p>You can add keywords to add additional descriptive information for your content. You can add multiple keywords by separating them with commas.</p>
              <p class="small text-muted">Example: Lymphatic system, biology, microbiology</p>
              <p><a href="/tags/oer" target="_blank">View the "OER" tag landing page</a>.</p>`,
              position: 'right',
              padding: 15,
            },
            {
              element: $('#edit-field-work-in-progress-wrapper'),
              content: `<h2>Options: Work in Progress (WIP)</h2>
              <p>Check this box if the content is not ready for use or unfinished in some way. Content that is marked as a Work in Progress will not be listed in the catalogue, but can be listed on your profile</p>`,
              position: 'right',
              padding: 15,
            },
            {
              element: $('#edit-field-show-in-catalogue-wrapper'),
              content: `<h2>Options: Show in Catalogue</h2>
              <p>Check this box if you want your content to be discoverable using the site's Catalogue.</p>`,
              position: 'right',
              padding: 15,
            },
            {
              element: $('#edit-field-display-in-profile-wrapper'),
              content: `<h2>Options: Show in Profile</h2>
              <p>Check this box if you want to feature this content on your profile. Other members will be able to visit your profile an view the content you have created. </p>`,
              position: 'right',
              padding: 15,
            },
            {
              element: $('#edit-field-h5p-content-0-h5p-content-editor'),
              content: `<h2>H5P Editor</h2>
              <p>This is the H5P Editor panel. You can use this editor to create/edit interactive H5P content.</p>
              <p>If this is your first time using H5P, you should check out the <a href="/pages/getting-started" target="_blank">Getting Started</a> documentation first.`,
              position: 'left',
              padding: 15,
            }
          ]);
        });

      }
    };

})(jQuery, Drupal, drupalSettings);